import numpy as np
import pandas as pd
import os
#from astropy.stats import bayesian_blocks as bb
#from astropy.stats import histogram as hi
import matplotlib
import matplotlib.pyplot as plt
import matplotlib.colors as mcolors
matplotlib.use('Agg')

files_training = os.listdir('age_met')
for f in files_training:
    CMD = pd.read_table('age_met/' + f, header = None, comment='#', delim_whitespace=True)
    mag = -CMD[0]
    col = CMD[1]
    mag = mag[(col > -1) & (col < 3)]
    col = col[(col > -1) & (col < 3)]
    gamma = 0.2
    plt.figure(figsize=(4,4)) #fuck inches and matplotlib
    plt.axis('off')
    u = plt.hist2d(col, mag, bins = 300, range = [[-1.0,3.0],[-8.0,8.0]], norm=mcolors.PowerNorm(gamma))
    plt.savefig('img_train/' + f + '.png', format = 'png', bbox_inches='tight', pad_inches=0.0)
    plt.close()

files_validation = os.listdir('val_ori')
for f in files_validation:
    CMD = pd.read_csv('val_ori/' + f, header = None)
    mag = -CMD[1]
    col = CMD[0]-CMD[1]
    mag = mag[(col > -1) & (col < 3)]
    col = col[(col > -1) & (col < 3)]
    gamma = 0.2
    plt.figure(figsize=(4,4)) #fuck inches and matplotlib
    plt.axis('off')
    u = plt.hist2d(col, mag, bins = 300, range = [[-1.0,3.0],[-25.0,-9.0]], norm=mcolors.PowerNorm(gamma))
    plt.savefig('img_val/' + f + '.png', format = 'png', bbox_inches='tight', pad_inches=0.0)
    plt.close()
